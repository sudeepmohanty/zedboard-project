/*
// This work contains the implementation of a real time audio processing
// system based on the Xilinx Zed board.
//
// File: main.cpp
//
// Authors:  SM	 Sudeep Mohanty
//			 KSG Kunal Sanjay Gokhale
//
// $Revision: Rev 1.0 $
//
// PURPOSE:  This file implements the main(). It is the start call point from
// 			 power up, initialises the required functions and runs in an infinite loop
//			 to capture the audio inputs and display the frequency response on
//			 the OLED screen. The intermediate processing of the signals has been done
//			 using the ARM core and the FPGA PL of the Zedboard. 
 *
 */
 
 
#include <stdio.h>
#include "platform.h"
#include "xparameters.h"
#include "xil_io.h"
#include "xbasic_types.h"
#include "audio.h"
#include "oled.h"
#include "sleep.h"
#include <stdlib.h>

#define SAMPLE_DATA_SIZE 128
#define MOVING_WINDOW_SIZE 8

int main()
{
	Xint16 audio_data[128];
	u8 *oled_equalizer_buf=(u8 *)malloc(128*sizeof(u8));
	Xil_Out32(OLED_BASE_ADDR,0xff);
	OLED_Init();			//oled init
	IicConfig(XPAR_XIICPS_0_DEVICE_ID);
	AudioPllConfig(); //enable core clock for ADAU1761
	AudioConfigure();

	Xint16 i = 0;
	Xint16 j = 0;
	Xint16 receivedData[128];
    Xint16 noiseMatrix[100][128];
    Xint16 noiseArray[128];
    Xint16 count = 100000;
    Xint16 XAvg[SAMPLE_DATA_SIZE];
    Xint16 averageMatrixFull;
    Xint16 averageMatrix[MOVING_WINDOW_SIZE][SAMPLE_DATA_SIZE];
    Xint16 avgMatrixRowIdx = 0;

    //set noise_array flag
    Xil_Out16((XPAR_MYPERIPHERAL_0_S_AXI_MEM0_BASEADDR + 4*130), 1);

    for(i=0;i<128;i++)
    {
    	Xil_Out16((XPAR_MYPERIPHERAL_0_S_AXI_MEM0_BASEADDR + 4*i), 0);
    	noiseArray[i] = 0;
    }

    //Xil_Out16((XPAR_MYPERIPHERAL_0_S_AXI_MEM0_BASEADDR + 4*130), 0);

    while(1)
    {
    	get_audio(audio_data);

    	for(i = 0; i < 128; i++)
    	{
    		Xil_Out16((XPAR_MYPERIPHERAL_0_S_AXI_MEM0_BASEADDR + 4*i), audio_data[i]);
    	}
    	Xil_Out16((XPAR_MYPERIPHERAL_0_S_AXI_MEM0_BASEADDR + 4*128), 1);

    	while(1)
    	{
    		if(Xil_In8((XPAR_MYPERIPHERAL_0_S_AXI_MEM0_BASEADDR + 4*129)) == 1)
    		{
    			//xil_printf("Flag worked!\n");
    			break;
    		}
    	}

    	for(i = 0; i < 128; i++)
    	{
    		noiseMatrix[j][i] = Xil_In16((XPAR_MYPERIPHERAL_0_S_AXI_MEM0_BASEADDR + 4*i));
    	}

        Xil_Out16((XPAR_MYPERIPHERAL_0_S_AXI_MEM0_BASEADDR + 4*128), 0);
    	Xil_Out16((XPAR_MYPERIPHERAL_0_S_AXI_MEM0_BASEADDR + 4*129), 0);

    	j++;
    	if(j > 100)
    	{
    		break;
    	}
    	//count--
    }

	for(i = 0; i < 128; i++)
	{
		for(j = 0; j < 100; j++)
		{
			if(noiseArray[i] < noiseMatrix[j][i]);
			{
				noiseArray[i] = noiseMatrix[j][i];
			}
		}
	}

	Xil_Out16((XPAR_MYPERIPHERAL_0_S_AXI_MEM0_BASEADDR + 4*130), 1);

    //populates noise_array
    for(i=0;i<128;i++)
    {
    	//xil_printf("noiseArray[%d] = %d\n", i, noiseArray[i]);
    	Xil_Out16((XPAR_MYPERIPHERAL_0_S_AXI_MEM0_BASEADDR + 4*i), (0));
    }

    //reset noise_array flag
    Xil_Out16((XPAR_MYPERIPHERAL_0_S_AXI_MEM0_BASEADDR + 4*130), 0);

    while(1)
    {
    	get_audio(audio_data);

    	for(i = 0; i < 128; i++)
    	{
    		//xil_printf("receivedData[%d] = %d\n", i, audio_data[i]);
    		Xil_Out16((XPAR_MYPERIPHERAL_0_S_AXI_MEM0_BASEADDR + 4*i), audio_data[i]);
    	}
    	Xil_Out16((XPAR_MYPERIPHERAL_0_S_AXI_MEM0_BASEADDR + 4*128), 1);

		while(1)
		{
			if(Xil_In8((XPAR_MYPERIPHERAL_0_S_AXI_MEM0_BASEADDR + 4*129)) == 1)
			{
				//xil_printf("Flag worked!\n");
				break;
			}
		}
		for(i = 0; i < 128; i++)
		{
			receivedData[i] = Xil_In16((XPAR_MYPERIPHERAL_0_S_AXI_MEM0_BASEADDR + 4*i));

			if(i <= 96 && i >= 32)
			{
				if(receivedData[i] > noiseArray[i])
				{
					receivedData[i] -= noiseArray[i];
				}
				receivedData[i] = ((float)receivedData[i]/65535.0) * 40 * 200;
			}
			else
			{
				if(receivedData[i] > noiseArray[i])
				{
					receivedData[i] -= noiseArray[i];
				}
				receivedData[i] = ((float)receivedData[i]/65535.0) * 40 * 8;
			}
			averageMatrix[avgMatrixRowIdx][i] = receivedData[i];
		}

		avgMatrixRowIdx = (avgMatrixRowIdx + 1) & 7;

		if(avgMatrixRowIdx == 7)
		{
			averageMatrixFull = 1;
		}

		if(averageMatrixFull)
		{
			// Do the average
			for(j = 0; j < SAMPLE_DATA_SIZE; j++)
			{
				for(i = 0; i < MOVING_WINDOW_SIZE; i++)
				{
					XAvg[j] += averageMatrix[i][j];
				}
				XAvg[j] = XAvg[j] >> 3;
				oled_equalizer_buf[j]=XAvg[j];
			}

			/* OLED Display */

			OLED_Clear();
			OLED_Equalizer_128(oled_equalizer_buf);
		}

		Xil_Out16((XPAR_MYPERIPHERAL_0_S_AXI_MEM0_BASEADDR + 4*128), 0);
		Xil_Out16((XPAR_MYPERIPHERAL_0_S_AXI_MEM0_BASEADDR + 4*129), 0);
    }
	return 0;
}
