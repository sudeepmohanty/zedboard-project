`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    02:40:26 10/08/2014 
// Design Name: 
// Module Name:    IP_FFT2_WRAPPER 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module IP_FFT2_WRAPPER(
	input clk,
	input start,
	input [15 : 0] xn_re,
	input [15 : 0] xn_im,
	input fwd_inv,
	input fwd_inv_we,
	input [13 : 0] scale_sch,
	input scale_sch_we,
	output rfd,
	output [6 : 0] xn_index,
	output busy,
	output edone,
	output done,
	output dv,
	output [6 : 0] xk_index,
	output [15 : 0] xk_re,
	output [15 : 0] xk_im
    );

	wire rfd1;
	wire [6 : 0] xn_index1;
	wire busy1;
	wire edone1;
	wire done1;
	wire dv1;
	wire [6 : 0] xk_index1;
	wire [15 : 0] xk_re1;
	wire [15 : 0] xk_im1;

	reg [15 : 0] output_re;
	reg [15 : 0] output_im;
	reg [31 : 0] mag_array[0:127];
	reg [15 : 0] noise_array[0:127];
	reg [15 : 0] avg_matrix[0:7][0:127];
	reg [7 : 0] avgMatrixFull; 
	reg [7 : 0] avgMatrixRowIdx;
	reg [15 : 0] xk_avg[0:127];
	reg [7 : 0] tempRowIdx;
	reg [31 : 0] temp_mag[0:127];
	reg [7 : 0] IP2Bus_Flag[3 : 0];
	reg [7 : 0] noise_array_flag[3 : 0];
	reg [7 : 0] averageMatrixFull;

IP_FFT2 your_instance_name (
  .clk(clk), // input clk
  .start(start), // input start
  .xn_re(xn_re), // input [15 : 0] xn_re
  .xn_im(xn_im), // input [15 : 0] xn_im
  .fwd_inv(fwd_inv), // input fwd_inv
  .fwd_inv_we(fwd_inv_we), // input fwd_inv_we
  .scale_sch(scale_sch), // input [13 : 0] scale_sch
  .scale_sch_we(scale_sch_we), // input scale_sch_we
  .rfd(rfd1), // output rfd
  .xn_index(xn_index1), // output [6 : 0] xn_index
  .busy(busy1), // output busy
  .edone(edone1), // output edone
  .done(done1), // output done
  .dv(dv1), // output dv
  .xk_index(xk_index1), // output [6 : 0] xk_index
  .xk_re(xk_re1), // output [15 : 0] xk_re
  .xk_im(xk_im1) // output [15 : 0] xk_im
);

	assign rfd = rfd1;
	assign xn_index = xn_index1;
	assign busy = busy1;
	assign edone = edone1;
	assign done = done1;
	assign dv = dv1;
	assign xk_index = xk_index1;
	assign xk_re = xk_re1;
	assign xk_im = xk_im1;

// Get output from FFT
always @ (posedge clk) begin
	if(dv == 1)
	begin
		if(xk_re[15:15] == 1)
		begin
			output_re = (~xk_re) + 1;
		end
		else
		begin
			output_re = xk_re;
		end
		if(xk_im[15:15] == 1)
		begin
			output_im = (~xk_im) + 1;
		end
		else
		begin
			output_im = xk_im;
		end
		
		temp_mag[xk_index] = ((output_re * output_re) + (output_im * output_im)) - noise_array[xk_index];
		
		//if( noise_array_flag[0] == 0 && ((temp_mag[xk_index] > 40) || (temp_mag[xk_index][31:31] == 1)))
		//begin	
			if(temp_mag[xk_index][31:31] == 1)
			begin
				avg_matrix[avgMatrixRowIdx][xk_index] = 0;
			end
			else
			begin
				avg_matrix[avgMatrixRowIdx][xk_index] = temp_mag[xk_index][15:0];
				//avg_matrix[avgMatrixRowIdx][xk_index] = 40;
				//avg_matrix[avgMatrixRowIdx][xk_index] = (temp_mag[xk_index][15:0]/65535)*40;
			end
		//end
		//else
		//begin
			//avg_matrix[avgMatrixRowIdx][xk_index] = temp_mag[xk_index][15:0];
		//end		
		
		if(avgMatrixRowIdx == 7 && xk_index == 127)
		begin
			avgMatrixFull = 1;
		end
		
		if(xk_index == 127)
		begin
			tempRowIdx = (avgMatrixRowIdx + 1) & 7;
			avgMatrixRowIdx = tempRowIdx;
		end

		if(noise_array_flag[0] == 0 && avgMatrixFull == 1)
		begin						
			xk_avg[xk_index] = (avg_matrix[0][xk_index] + avg_matrix[1][xk_index] + avg_matrix[2][xk_index] + avg_matrix[3][xk_index] + avg_matrix[4][xk_index] + avg_matrix[5][xk_index] + avg_matrix[6][xk_index] + avg_matrix[7][xk_index]) >> 3;
			//xk_avg[xk_index] = temp_mag[xk_index][15:0];
			IP2Bus_Flag[0] = 1;		
		end
		else
		begin
			xk_avg[xk_index] = temp_mag[xk_index][15:0];
			IP2Bus_Flag[0] = 1;	
		end
	end
end

endmodule
