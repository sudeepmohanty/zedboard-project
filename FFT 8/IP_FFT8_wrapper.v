`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    02:58:50 10/30/2014 
// Design Name: 
// Module Name:    IP_FFT8_wrapper 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////



module IP_FFT8_wrapper(
input aclk,
input [31 : 0] s_axis_data_tdata,
input s_axis_data_tvalid,
output s_axis_data_tready,
output [31 : 0] m_axis_data_tdata_o,
output m_axis_data_tvalid,
input m_axis_data_tready
);
	
	//wire s_axis_data_tready1; // output s_axis_data_tready
	//wire m_axis_data_tdata1; // output [31 : 0] m_axis_data_tdata
	//wire m_axis_data_tvalid1; // output m_axis_data_tvalid
	//wire m_axis_data_tlast1; // output m_axis_data_tlast   
	
wire [47:0] m_axis_data_tdata;
wire [15:0] r_ip;
reg [15:0] r_tc=0;
reg [15:0] i_tc=0;
wire [15:0] i_ip;
assign r_ip=m_axis_data_tdata [15: 0];
assign i_ip=m_axis_data_tdata [39: 24];

always @ (m_axis_data_tdata [47:0])
begin
if (r_ip[15]==1'b1)
	begin
      r_tc[15:0] = (~r_ip[15:0] + 1'b1);
    end
    else
    begin
      r_tc[15:0] = r_ip;
	 end
	 if (i_ip[15]==1'b1)
	begin
      i_tc[15:0] = (~i_ip[15:0] + 1'b1);
    end
    else
    begin
      i_tc[15:0] = i_ip;
    end
	end
	
assign m_axis_data_tdata_o= (r_tc*r_tc) + (i_tc*i_tc);


IP_FFT_8 your_instance_name (
  .aclk(aclk), // input aclk
  .s_axis_config_tdata(0), // input [15 : 0] s_axis_config_tdata
  .s_axis_config_tvalid(0), // input s_axis_config_tvalid
  .s_axis_config_tready(), // output s_axis_config_tready
  .s_axis_data_tdata(s_axis_data_tdata), // input [31 : 0] s_axis_data_tdata
  .s_axis_data_tvalid(s_axis_data_tvalid), // input s_axis_data_tvalid
  .s_axis_data_tready(s_axis_data_tready), // output s_axis_data_tready
  .s_axis_data_tlast(0), // input s_axis_data_tlast
  .m_axis_data_tdata(m_axis_data_tdata), // output [31 : 0] m_axis_data_tdata
  .m_axis_data_tvalid(m_axis_data_tvalid), // output m_axis_data_tvalid
  .m_axis_data_tready(m_axis_data_tready), // input m_axis_data_tready
  .m_axis_data_tlast(), // output m_axis_data_tlast
  .event_frame_started(), // output event_frame_started
  .event_tlast_unexpected(), // output event_tlast_unexpected
  .event_tlast_missing(), // output event_tlast_missing
  .event_status_channel_halt(), // output event_status_channel_halt
  .event_data_in_channel_halt(), // output event_data_in_channel_halt
  .event_data_out_channel_halt() // output event_data_out_channel_halt
);


	//assign s_axis_data_tready = s_axis_data_tready1; // output s_axis_data_tready
	//assign m_axis_data_tdata = m_axis_data_tdata1; // output [31 : 0] m_axis_data_tdata
	//assign m_axis_data_tvalid = m_axis_data_tvalid1; // output m_axis_data_tvalid
	//assign m_axis_data_tlast = m_axis_data_tlast1; // output m_axis_data_tlast
   

endmodule
